import jwt
import requests
from flask import g, request
from werkzeug.exceptions import Unauthorized
from werkzeug.utils import cached_property
from . import properties


def get_decoded_jwt():
    if "decoded_jwt" not in g:
        g.decoded_jwt = _decode_token()

    return g.decoded_jwt


def _decode_token():
    token = _parse_auth_token()
    try:
        payload = jwt.decode(
            token,
            properties.public_key,
            algorithms="RS256",
            audience=properties.audience,
            issuer="convergysiq",
        )
    except jwt.ExpiredSignatureError:
        raise Unauthorized("token is expired")
    except jwt.InvalidAudienceError:
        raise Unauthorized("invalid audience")
    except jwt.InvalidIssuerError:
        raise Unauthorized("invalid issuer")
    except jwt.InvalidTokenError:
        raise Unauthorized("invalid token")
    return payload


def _parse_auth_token():
    auth = request.headers.get("authorization", None)
    if not auth:
        raise Unauthorized("missing required 'authorization' header")

    parts = auth.split()
    if parts[0].lower() != "bearer":
        raise Unauthorized("authorization header must be a Bearer token")

    if len(parts) == 1:
        raise Unauthorized("authorization header missing token")

    return parts[1]
