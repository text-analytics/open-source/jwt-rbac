from enum import IntFlag


class Permission(IntFlag):
    CREATE = 0b1000
    READ = 0b0100
    UPDATE = 0b0010
    DELETE = 0b0001
    NONE = 0b0000
    ALL = 0b1111
