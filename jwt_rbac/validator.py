from typing import Dict, List, Optional

from flask import request
from werkzeug.exceptions import BadRequest, Forbidden

from .decoder import get_decoded_jwt
from .parse_tenant import parse_tenant
from .permission import Permission
from .tenant_info import TenantInfo


class Validator:

    def __init__(self, service_name, resource_name):
        self.service_name = service_name
        self.resource_name = resource_name

    def can_create(self, tenant_uuid: str = None) -> TenantInfo:
        return self.has_permissions([Permission.CREATE], tenant_uuid)

    def can_read(self, tenant_uuid: str = None) -> TenantInfo:
        return self.has_permissions([Permission.READ], tenant_uuid)

    def can_update(self, tenant_uuid: str = None) -> TenantInfo:
        return self.has_permissions([Permission.UPDATE], tenant_uuid)

    def can_delete(self, tenant_uuid: str = None) -> TenantInfo:
        return self.has_permissions([Permission.DELETE], tenant_uuid)

    def has_permissions(self, permissions: List[Permission], tenant_uuid: str = None) -> TenantInfo:
        if not permissions:
            raise ValueError('permissions are required to verify permissions?!')
        
        try:
            tenant_uuid = tenant_uuid or request.args.get('tenant')
        except RuntimeError:
            # A RuntimeError is thrown if we are outside of flask.
            # We'd rather throw the more helpful BadRequest below.
            pass
        
        if not tenant_uuid:
            raise BadRequest("Missing or blank required query parameter 'tenant'")

        decoded_jwt = get_decoded_jwt()
        tenant_info = parse_tenant(tenant_uuid, decoded_jwt)
        tenant = decoded_jwt['permissions'][tenant_info.uuid]

        requested_permissions = Permission.NONE
        for permission in permissions:
            requested_permissions |= permission

        actual_permissions = Permission.NONE

        try:
            actual_permissions |= tenant[self.service_name][self.resource_name]
        except KeyError:
            pass

        # also check special all service and resource
        try:
            actual_permissions |= tenant['all']['all']
        except KeyError:
            pass

        if (requested_permissions & actual_permissions) != requested_permissions:
            raise Forbidden()

        return tenant_info
