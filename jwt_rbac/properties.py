import os

import requests

from .logger import log

# Declare vars so that IDEs can suggest them
audience = None
padlock_url = None
public_key = None

# But delete them from the module so they aren't actually initialized
del globals()["audience"]
del globals()["padlock_url"]
del globals()["public_key"]


def set_rbac_vars(audience=None, padlock_url=None):
    if audience:
        globals()["audience"] = audience
    if padlock_url:
        globals()["padlock_url"] = padlock_url


# __get_attr__ is called if a variable is not found through normal means
def __getattr__(name):

    if name == "public_key":
        pk = _get_public_key()
        # "globals()" really means "variables in the module." Setting the
        # public_key in globals ensures that this code block will only be run
        # once, the first time the public_key is accessed. After that it is
        # retrived like a normal module variable.
        globals()["public_key"] = pk
        return pk

    if name == "audience" or name == "padlock_url":
        raise AttributeError(
            f"set_rbac_vars({name}=<value>) must be called before the jwt_rbac lib is used"
        )

    raise AttributeError(f"module '{__name__}' has no attribute '{name}'")


def _get_public_key():
    url = f"{padlock_url}/v0/tokens/public-key"
    log.info(f"obtaining public_key from {url}")
    return requests.get(url).json()
