import logging

_name = "jwt_rbac"
log = logging.getLogger(_name)

# If there are no handlers, add one to write to console.
if not log.hasHandlers():
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter(f"[%(asctime)s] [%(levelname)s] [{_name}] %(message)s", datefmt="%m-%d %H:%M:%S")
    ch.setFormatter(formatter)
    log.setLevel(logging.DEBUG)
    log.addHandler(ch)
