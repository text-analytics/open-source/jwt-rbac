from typing import Dict, List

import requests
from werkzeug.exceptions import Forbidden

from . import properties
from .decoder import get_decoded_jwt
from .logger import log
from .tenant_info import TenantInfo


def register_resources(service_name: str, resources: List[Dict]) -> requests.Response:
    for resource in resources:
        resource.update({"service": service_name})

    url = f"{properties.padlock_url}/v0/resources"
    log.debug(f"registering resources")
    return requests.post(url, json=resources)


def get_user_info(user_uuid: str, auth: str):
    url = f"{properties.padlock_url}/v0/users/{user_uuid}"
    log.debug(f"getting user info")
    return requests.get(url, headers={"Authorization": auth}).json()


def is_tenant_owner(tenant_uuid: str = None) -> TenantInfo:
    decoded_jwt = get_decoded_jwt()
    try:
        tenant = decoded_jwt["permissions"][tenant_uuid]
    except KeyError:
        raise Forbidden

    if decoded_jwt["sub"] != tenant["owner"]:
        raise Forbidden("only tenant the owner can perform this action")

    return TenantInfo(tenant_uuid, tenant["name"])
