from typing import Dict, Optional

from flask import request
from werkzeug.exceptions import BadRequest, Forbidden

from .tenant_info import TenantInfo


def parse_tenant(tenant_uuid: str, decoded_jwt: Dict) -> TenantInfo:
    tenant_id = tenant_uuid.strip()

    try:
        tenant = decoded_jwt["permissions"][tenant_id]
    except KeyError:
        raise Forbidden

    return TenantInfo(tenant_id, tenant["name"])
