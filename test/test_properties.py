import pytest
from jwt_rbac import properties
from unittest.mock import patch


def test_audience():
    with pytest.raises(AttributeError, match=r"set_rbac_vars"):
        properties.audience

    properties.set_rbac_vars(audience="test_case")

    assert properties.audience == "test_case"


def test_padlock_url():
    with pytest.raises(AttributeError, match=r"set_rbac_vars"):
        properties.padlock_url

    properties.set_rbac_vars(padlock_url="http://test_case")

    assert properties.padlock_url == "http://test_case"


@patch("jwt_rbac.properties.requests.get")
def test_public_key(mock_get):
    # Set up
    properties.set_rbac_vars(padlock_url="http://test_case")
    mock_get.return_value.json.return_value = "public_key"

    # Assert that padlock has not been contacted yet
    mock_get.assert_not_called()

    # Fetch the public key (lazily)
    assert properties.public_key == "public_key"
    mock_get.assert_called_once()

    # Reset the mock and fetch the key again
    mock_get.reset_mock()
    assert properties.public_key == "public_key"
    mock_get.assert_not_called()
