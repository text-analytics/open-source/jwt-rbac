from unittest.mock import patch

import jwt
import flask
import pytest
from werkzeug.exceptions import Unauthorized

from jwt_rbac import decoder

app = flask.Flask(__name__)

@pytest.fixture
def mock_props():
    with patch("jwt_rbac.decoder.properties") as mp:
        mp.audience = "test_audience"
        mp.public_key = "test_public_key"
        yield mp


@patch("jwt_rbac.decoder.jwt.decode", return_value="test_jwt")
def test_decoder(mock_decode, mock_props):
    with app.test_request_context(headers={"Authorization": "Bearer eyJhbGc"}):
        assert decoder.get_decoded_jwt() == "test_jwt"
        mock_decode.assert_called_once()
        args = mock_decode.call_args[0]
        kwargs = mock_decode.call_args[1]
        assert args[0] == "eyJhbGc"
        assert args[1] == "test_public_key"
        assert kwargs["audience"] == "test_audience"

        # Test that subsequent calls do not recalcuate the jwt
        mock_decode.reset_mock()
        assert decoder.get_decoded_jwt() == "test_jwt"
        mock_decode.assert_not_called()


def test_decoder_no_header():
    with pytest.raises(Unauthorized):
        with app.test_request_context():
            decoder.get_decoded_jwt()


def test_decoder_no_bearer():
    with pytest.raises(Unauthorized):
        with app.test_request_context(headers={"Authorization": "eyJhbGc"}):
            decoder.get_decoded_jwt()


def test_decoder_no_space():
    with pytest.raises(Unauthorized):
        with app.test_request_context(headers={"Authorization": "BearereyJhbGc"}):
            decoder.get_decoded_jwt()


def test_decoder_no_token():
    with pytest.raises(Unauthorized):
        with app.test_request_context(headers={"Authorization": "Bearer"}):
            decoder.get_decoded_jwt()


@patch("jwt_rbac.decoder.jwt.decode")
def test_decode_expired(mock_decode, mock_props):
    mock_decode.side_effect = jwt.ExpiredSignatureError

    with pytest.raises(Unauthorized):
        with app.test_request_context(headers={"Authorization": "Bearer eyJhbGc"}):
                decoder.get_decoded_jwt()


@patch("jwt_rbac.decoder.jwt.decode")
def test_decode_invalid_audience(mock_decode, mock_props):
    mock_decode.side_effect = jwt.InvalidAudienceError

    with pytest.raises(Unauthorized):
        with app.test_request_context(headers={"Authorization": "Bearer eyJhbGc"}):
                decoder.get_decoded_jwt()


@patch("jwt_rbac.decoder.jwt.decode")
def test_decode_invalid_issuer(mock_decode, mock_props):
    mock_decode.side_effect = jwt.InvalidIssuerError

    with pytest.raises(Unauthorized):
        with app.test_request_context(headers={"Authorization": "Bearer eyJhbGc"}):
                decoder.get_decoded_jwt()


@patch("jwt_rbac.decoder.jwt.decode")
def test_decode_invalid_token(mock_decode, mock_props):
    mock_decode.side_effect = jwt.InvalidTokenError

    with pytest.raises(Unauthorized):
        with app.test_request_context(headers={"Authorization": "Bearer eyJhbGc"}):
                decoder.get_decoded_jwt()
