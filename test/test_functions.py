from unittest.mock import patch

import pytest
from werkzeug.exceptions import Forbidden

from jwt_rbac import Permission, get_user_info, is_tenant_owner, register_resources

TENANT_ID = "test_tenant_id"
TENANT_NAME = "test_tenant_name"
RESOURCE_NAME = "test_resource"
SERVICE_NAME = "test_service"
PADLOCK_URL = "test_padlock_url"


@patch("jwt_rbac.functions.get_decoded_jwt")
def test_is_tenant_owner(mock_decode):
    subject_uuid = "SUBJECT_UUID"
    mock_decode.return_value = {
        "sub": subject_uuid,
        "permissions": {TENANT_ID: {"name": TENANT_NAME, "owner": subject_uuid}},
    }

    is_tenant_owner(TENANT_ID)


@patch("jwt_rbac.functions.get_decoded_jwt")
def test_is_tenant_owner_Not_Owner(mock_decode):
    subject_uuid = "SUBJECT_UUID"
    mock_decode.return_value = {
        "sub": subject_uuid,
        "permissions": {TENANT_ID: {"name": TENANT_NAME, "owner": "NOT_SUBJECT_UUID"}},
    }

    with pytest.raises(Forbidden):
        is_tenant_owner(TENANT_ID)


@patch("jwt_rbac.functions.get_decoded_jwt")
def test_is_tenant_owner_no_tenant_permissions(mock_decode):
    subject_uuid = "SUBJECT_UUID"
    mock_decode.return_value = {
        "sub": subject_uuid,
        "permissions": {"NOT_TENANT_ID": {"name": TENANT_NAME, "owner": subject_uuid}},
    }

    with pytest.raises(Forbidden):
        is_tenant_owner(TENANT_ID)


@patch("jwt_rbac.functions.requests.post")
@patch("jwt_rbac.functions.properties")
def test_register_resources(mock_props, mock_post):
    mock_props.padlock_url = PADLOCK_URL
    resources = [
        {"name": "attribute", "description": "Attribute"},
        {"name": "categorize", "description": "Categorize"},
    ]
    register_resources(SERVICE_NAME, resources)

    mock_post.assert_called_once()
    args, kwargs = mock_post.call_args
    assert len(args) == 1
    assert PADLOCK_URL in args[0]

    assert len(kwargs) == 1
    assert "json" in kwargs
    json = kwargs["json"]
    assert len(json) == 2
    for res in json:
        assert "service" in res


@patch("jwt_rbac.functions.requests.get")
@patch("jwt_rbac.functions.properties")
def test_get_user_info(mock_props, mock_get):
    mock_props.padlock_url = PADLOCK_URL
    user_id = "test_user_id"
    auth = "test_auth"
    get_user_info(user_id, auth)

    mock_get.assert_called_once()

    args, kwargs = mock_get.call_args
    assert len(args) == 1
    assert PADLOCK_URL in args[0]
    assert user_id in args[0]

    assert len(kwargs) == 1
    assert "headers" in kwargs
    headers = kwargs["headers"]
    assert len(headers) == 1
    assert "Authorization" in headers
    assert headers["Authorization"] == auth

