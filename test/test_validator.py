from unittest.mock import patch

import flask
import pytest
from werkzeug.exceptions import BadRequest, Forbidden, Unauthorized

from jwt_rbac import Permission, Validator

app = flask.Flask(__name__)
TENANT_ID = "test_tenant_id"
TENANT_NAME = "test_tenant_name"
RESOURCE_NAME = "test_resource"
SERVICE_NAME = "test_service"


@pytest.fixture
def validator():
    return Validator(SERVICE_NAME, RESOURCE_NAME)


def test_can_create(validator):
    with patch.object(validator, "has_permissions") as mock_has_permissions:
        validator.can_create(TENANT_ID)

    mock_has_permissions.assert_called_once_with([Permission.CREATE], TENANT_ID)


def test_can_read(validator):
    with patch.object(validator, "has_permissions") as mock_has_permissions:
        validator.can_read(TENANT_ID)

    mock_has_permissions.assert_called_once_with([Permission.READ], TENANT_ID)


def test_can_update(validator):
    with patch.object(validator, "has_permissions") as mock_has_permissions:
        validator.can_update(TENANT_ID)

    mock_has_permissions.assert_called_once_with([Permission.UPDATE], TENANT_ID)


def test_can_delete(validator):
    with patch.object(validator, "has_permissions") as mock_has_permissions:
        validator.can_delete(TENANT_ID)

    mock_has_permissions.assert_called_once_with([Permission.DELETE], TENANT_ID)


@patch("jwt_rbac.validator.get_decoded_jwt")
def test_has_read_only_permissions(mock_decode, validator):
    mock_decode.return_value = {
        "permissions": {
            TENANT_ID: {
                "name": TENANT_NAME,
                SERVICE_NAME: {RESOURCE_NAME: Permission.READ},
            }
        }
    }

    validator.has_permissions([Permission.READ], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.ALL], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.CREATE], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.UPDATE], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.DELETE], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.CREATE, Permission.UPDATE], TENANT_ID)


@patch("jwt_rbac.validator.get_decoded_jwt")
def test_has_create_delete_permissions(mock_decode, validator):
    mock_decode.return_value = {
        "permissions": {
            TENANT_ID: {
                "name": TENANT_NAME,
                SERVICE_NAME: {RESOURCE_NAME: Permission.CREATE | Permission.DELETE},
            }
        }
    }

    validator.has_permissions([Permission.CREATE], TENANT_ID)
    validator.has_permissions([Permission.DELETE], TENANT_ID)
    validator.has_permissions([Permission.CREATE, Permission.DELETE], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.READ], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.ALL], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.UPDATE], TENANT_ID)


@patch("jwt_rbac.validator.get_decoded_jwt")
def test_has_all_permissions(mock_decode, validator):
    mock_decode.return_value = {
        "permissions": {
            TENANT_ID: {
                "name": TENANT_NAME,
                SERVICE_NAME: {RESOURCE_NAME: Permission.ALL},
            }
        }
    }

    validator.has_permissions([Permission.ALL], TENANT_ID)
    validator.has_permissions([Permission.READ], TENANT_ID)
    validator.has_permissions([Permission.CREATE], TENANT_ID)
    validator.has_permissions([Permission.UPDATE], TENANT_ID)
    validator.has_permissions([Permission.DELETE], TENANT_ID)
    validator.has_permissions([Permission.CREATE, Permission.UPDATE], TENANT_ID)


@patch("jwt_rbac.validator.get_decoded_jwt")
def test_has_admin_permissions(mock_decode, validator):
    mock_decode.return_value = {
        "permissions": {
            TENANT_ID: {"name": TENANT_NAME, "all": {"all": Permission.ALL}}
        }
    }

    validator.has_permissions([Permission.ALL], TENANT_ID)
    validator.has_permissions([Permission.READ], TENANT_ID)
    validator.has_permissions([Permission.CREATE], TENANT_ID)
    validator.has_permissions([Permission.UPDATE], TENANT_ID)
    validator.has_permissions([Permission.DELETE], TENANT_ID)
    validator.has_permissions([Permission.CREATE, Permission.UPDATE], TENANT_ID)


@patch("jwt_rbac.validator.get_decoded_jwt")
def test_has_read_admin_update_resource_permissions(mock_decode, validator):
    mock_decode.return_value = {
        "permissions": {
            TENANT_ID: {
                "name": TENANT_NAME,
                SERVICE_NAME: {RESOURCE_NAME: Permission.UPDATE},
                "all": {"all": Permission.READ},
            }
        }
    }

    validator.has_permissions([Permission.READ], TENANT_ID)
    validator.has_permissions([Permission.UPDATE], TENANT_ID)
    validator.has_permissions([Permission.READ, Permission.UPDATE], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.ALL], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.CREATE], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.DELETE], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.CREATE, Permission.UPDATE], TENANT_ID)

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.CREATE, Permission.DELETE], TENANT_ID)


@patch("jwt_rbac.validator.get_decoded_jwt")
def test_has_permissions_invalid_tenant(mock_decode, validator):
    mock_decode.return_value = {
        "permissions": {
            "INVALID_TENANT_ID": {
                "name": TENANT_NAME,
                SERVICE_NAME: {RESOURCE_NAME: Permission.ALL},
            }
        }
    }

    with pytest.raises(Forbidden):
        validator.has_permissions([Permission.ALL], TENANT_ID)


def test_has_permissions_no_tenant_id(validator):
    with pytest.raises(BadRequest):
        validator.has_permissions([Permission.ALL])


def test_has_permissions_no_tenant_id_in_flask_request(validator):
    with app.test_request_context():
        with pytest.raises(BadRequest):
            validator.has_permissions([Permission.ALL])


@patch("jwt_rbac.validator.get_decoded_jwt")
def test_has_permissions_tenant_id_from_request(mock_decode, validator):
    with app.test_request_context(query_string ={"tenant": TENANT_ID}):
        mock_decode.return_value = {
            "permissions": {
                TENANT_ID: {"name": TENANT_NAME, "all": {"all": Permission.ALL}}
            }
        }

        validator.has_permissions([Permission.ALL])
    

@patch("jwt_rbac.validator.get_decoded_jwt")
def test_has_permissions_None(mock_decode, validator):
    mock_decode.return_value = {
        "permissions": {
            TENANT_ID: {
                "name": TENANT_NAME,
                SERVICE_NAME: {RESOURCE_NAME: Permission.ALL}
            }
        }
    }

    with pytest.raises(ValueError):
        validator.has_permissions(None, TENANT_ID)


@patch("jwt_rbac.validator.get_decoded_jwt")
def test_has_permissions_Empty(mock_decode, validator):
    mock_decode.return_value = {
        "permissions": {
            TENANT_ID: {
                "name": TENANT_NAME,
                SERVICE_NAME: {RESOURCE_NAME: Permission.ALL}
            }
        }
    }

    with pytest.raises(ValueError):
        validator.has_permissions([], TENANT_ID)
