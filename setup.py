from setuptools import setup

setup(
    name='jwt-rbac',
    version='2.0.0',
    packages=['jwt_rbac'],
    url='https://gitlab.com/text-analytics/open-source/jwt-rbac',
    license='MIT',
    author='David Clutter, Nick Messersmith',
    author_email='david.clutter@convergys.com, nicholas.messersmith@convergys.com',
    description='JWT RBAC Permissions Parser',
    install_requires=[
        'flask',
        'requests',
        'pyjwt',
        'cryptography'
    ]
)
